#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <IE.au3>
#Region ### START Koda GUI section ### Form=
$Form1 = GUICreate("Form1", 615, 437, -986, 412)
_IEErrorHandlerRegister()
$Input1 = GUICtrlCreateInput("http:\\", 8, 8, 601, 21)
$go = GUICtrlCreateButton("go", 8, 40, 75, 25)
$oIE = _IECreateEmbedded()
GUICtrlCreateObj($oIE, 8, 72, 584, 328)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

WinMove ("Form1","",0,0);



While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		 Case $Input1

		 Case $go
		   _SetUserAgent('Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25')
			_IENavigate($oIE,GUICtrlRead($Input1))
	EndSwitch
WEnd


Func _SetUserAgent($agent)
    $agentLen = StringLen($agent)
    Dim $tBuff = DllStructCreate("char["&$agentLen&"]")
    DllStructSetData($tBuff, 1, $agent)
    $chk_UrlMkSetSessionOption = DllCall("urlmon.dll", "long", "UrlMkSetSessionOption", "dword", 0x10000001, "ptr", DllStructGetPtr($tBuff), "dword", $agentLen, "dword", 0)
EndFunc